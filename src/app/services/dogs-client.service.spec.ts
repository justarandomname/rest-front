import { TestBed } from '@angular/core/testing';

import { DogsClientService } from './dogs-client.service';

describe('DogsClientService', () => {
  let service: DogsClientService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DogsClientService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
