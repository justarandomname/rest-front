import { Injectable } from '@angular/core';
import {HttpClient, HttpClientModule, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DogsClientService {

  constructor(private httpClient: HttpClient) { }

  public getDog(id: string): Observable<RootObject>{
    const params1 = new HttpParams().set('id', id);

    return this.httpClient.get<RootObject>('http://localhost:8080/api/v1/shelter/dog', {params: params1});
  }
}

export interface RootObject {
  name: string;
  birthDate: string;
  sex: string;
}
