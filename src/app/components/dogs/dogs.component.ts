import { Component, OnInit } from '@angular/core';
import {DogsClientService, RootObject} from '../../services/dogs-client.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-dogs',
  templateUrl: './dogs.component.html',
  styleUrls: ['./dogs.component.css']
})
export class DogsComponent implements OnInit {

  messageForUser: string;
  rootObject: RootObject;

  constructor(private dogsClientService: DogsClientService) { }

  ngOnInit(): void {
  }

  sayHello(value: string) {
    this.messageForUser = 'czesc ' + value;
  }

  returnDog(dogId: string) {
    this.dogsClientService.getDog(dogId)
      .subscribe(value => this.rootObject = value);
  }
}
